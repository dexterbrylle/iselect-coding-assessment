#!/usr/bin/env node
'use strict'

const clc = require('cli-color')
const app = require('./index')

const file = process.argv[2]

app.runSimulation(file, (err, bot) => {
  if (err) {
    console.log(clc.white.bgRed('ERROR:') + ' ' + clc.red(err.message))
    return false
  }

  if (!bot.isPlaced) {
    console.log(clc.yellow('Robot was never placed on the table.'))
  }
  console.log(clc.white('=========================='))
  console.log(clc.blue('Simulation has finished.'))
  console.log(clc.white('=========================='))
})
