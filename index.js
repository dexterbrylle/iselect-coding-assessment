'use strict'

const Robot = require('./lib/robot')
const FileReader = require('./lib/file')
const Parser = require('./lib/parser')

const robot = new Robot()
const fileReader = new FileReader()
const parser = new Parser()

const app = {}

app.parseFile = async filename => {
  try {
    const file = await fileReader.readFile(filename)
    if (file) {
      const instructionList = parser.parseArgs(file)
      return instructionList
    }
  } catch (e) {
    console.log(e)
    return new Error(e)
  }
}

app.runSimulation = async (filename, callback) => {
  const instructionList = app.parseFile(filename)

  if (instructionList) {
    callback(null, robot.runInstructions(instructionList))
  }
}

module.exports = app
