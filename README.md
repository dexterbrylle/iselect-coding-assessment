# Tabletop robot simulator

The application is a simulation of a robot moving on a square table top, of dimensions 5 units x 5 units. There are no other obstructions on the table surface. The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement that would result in the robot falling from the table must be prevented, however further valid movement commands must still be allowed.
Create a console application that can read in commands of the following form:

PLACE X,Y,F
MOVE
LEFT
RIGHT
REPORT

## Installation and Running

* Install dependencies first

```sh
$ npm install
```

* Place bot commands in `instructions.txt`

* Run bot simulation via

```sh
$ node bot.js --- instructions.txt
```

## Tests

To run tests

```sh
$ npm run test
```
