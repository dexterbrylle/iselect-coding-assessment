'use strict'

class Parser {
  parseArgs (args) {
    if (!args.length) {
      return new Error('No arguments were passed.')
    }

    const parsedArgsArray = args.split('\n')
      .map(instruction => {
        return instruction.toLowerCase()
      })
      .reduce((instructions, rawInstruction) => {
        const parsedInstruction = this.parseInstruction(rawInstruction)

        if (parsedInstruction) {
          instructions.push(parsedInstruction)
        }

        return instructions
      })

    if (!parsedArgsArray.length) {
      return new Error('Invalid instructions passed.')
    }

    return parsedArgsArray
  }

  validDirections () {
    return ['north', 'south', 'east', 'west']
  }

  parseInstruction (rawString) {
    let instructionObj
    const multipleWordInstructionsList = rawString.split(' ')

    if (multipleWordInstructionsList.length > 1 &&
      multipleWordInstructionsList[0] === 'place') {
      instructionObj = this.parsePlaceInstruction(multipleWordInstructionsList)
    } else {
      instructionObj = this.parseSingleInstruction(multipleWordInstructionsList)
    }

    if (instructionObj) {
      return instructionObj
    }
  }

  parsePlaceInstruction (placeList) {
    const placeListArray = placeList[1].split(',')

    const x = parseInt(placeListArray[0], 10)
    const y = parseInt(placeListArray[1], 10)
    const direction = placeListArray[2]

    if (!isNaN(x) && !isNaN(y) && (this.validDirections.indexOf(direction) > -1)) {
      return {
        command: 'place',
        args: [x, y, direction]
      }
    } else {
      return null
    }
  }

  parseSingleInstruction (instructionString) {
    switch (instructionString) {
      case 'move':
        return {
          command: 'move'
        }
      case 'left':
        return {
          command: 'turn',
          args: 'left'
        }
      case 'right':
        return {
          command: 'turn',
          args: 'right'
        }
      case 'report':
        return {
          command: 'report'
        }
      default:
        return null
    }
  }
}

module.exports = Parser
