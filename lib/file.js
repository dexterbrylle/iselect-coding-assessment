'use strict'

/**
 * Read file input
 * @param {String} filename input filename
*/

const fs = require('fs')

class FileReader {
  validateInput (args) {
    const argsArray = args.split('.')

    if (argsArray.length === 1 && argsArray[0] === args) {
      return new Error('A valid txt file is required.')
    }

    if (argsArray[argsArray.length - 1] !== 'txt') {
      return new Error('Only txt files are accepted. Please check your file type.')
    }

    return args
  }

  async readFile (filename) {
    if (!this.validateInput(filename)) {
      return false
    }

    try {
      var file = await fs.readFile(filename)
      return file
    } catch (e) {
      return new Error(e)
    }
  }
}
module.exports = FileReader
