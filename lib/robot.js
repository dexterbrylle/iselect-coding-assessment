'use strict'

const clc = require('cli-color')

const directionMap = {
  north: {
    value: 'north',
    left: 'west',
    right: 'east'
  },
  east: {
    value: 'east',
    left: 'north',
    right: 'south'
  },
  south: {
    value: 'south',
    left: 'east',
    right: 'west'
  },
  west: {
    value: 'west',
    left: 'south',
    right: 'north'
  }
}

class Robot {
  constructor () {
    this.isPlaced = false
    this.position = {
      x: null,
      y: null
    }
    this.direction = null
  }

  tableSize () {
    return {
      x: 5,
      y: 5
    }
  }

  place (params) {
    const x = params[0]
    const y = params[1]
    const direction = directionMap[params[2]].value

    if (x > this.tableSize.x || y > this.tableSize.y) {
      return this
    }

    this.isPlaced = true
    this.position.x = x
    this.position.y = y
    this.direction = direction

    return this
  }

  move () {
    if (!this.isPlaced) {
      return this
    }

    let x = this.position.x
    let y = this.position.y

    switch (this.direction) {
      case 'north':
        if (++y < this.tableSize.y) {
          this.position = { x: x, y: y }
        }
        break
      case 'east':
        if (++x < this.tableSize.x) {
          this.position = { x: x, y: y }
        }
        break
      case 'south':
        if (--y >= 0) {
          this.position = { x: x, y: y }
        }
        break
      case 'west':
        if (--x >= 0) {
          this.position = { x: x, y: y }
        }
        break
      default:
        break
    }

    return this
  }

  turn (direction) {
    if (!this.isPlaced) {
      return this
    }

    const resultDirection = directionMap[this.direction][direction]

    if (resultDirection) {
      this.direction = resultDirection
    }

    return this
  }

  report () {
    if (!this.isPlaced) {
      return this
    }

    console.log(clc.greenBright('REPORT') + ' ' + [this.position.x, this.position.y, this.direction.toUpperCase()].join(','))

    return this
  }

  runInstructions (instructionList) {
    let instruction
    let robot = this

    for (var i = 0; i < instructionList.length; i++) {
      instruction = instructionList[i]
      if (instruction.args) {
        robot = this[instruction.command](instruction.args)
      } else {
        robot = this[instruction.command]()
      }
    }

    return robot
  }
}

module.exports = Robot
